using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Networking;
using SimpleJSON;
using System;
using System.IO;
using System.Text;
using System.Net;

using UnityEngine.SceneManagement;

public class CargarElementos : MonoBehaviour{

    public static string path = "./file/noConnection/";

    public static JSONNode getModulos()
    {     
        Debug.Log("Paso por el metodo getModulos");
        string pathModulos = path +"listaModulos.txt";
        JSONNode data = JSON.Parse(File.ReadAllText(pathModulos));
        return data ;  
            
    }

    public static JSONNode getUnidades()
    {     
        Debug.Log("Paso por el metodo getUnidades");
        string pathUnidades = path +"listaUnidades.txt";
        JSONNode data = JSON.Parse(File.ReadAllText(pathUnidades));
        return data ;  
            
    }

    public static JSONNode getPreguntas()
    {     
        Debug.Log("Paso por el metodo getPreguntas");
        string pathPreguntas = path +"listaPreguntas.txt";
        JSONNode data = JSON.Parse(File.ReadAllText(pathPreguntas));
        return data ;  
            
    }
}
