﻿﻿using System.Collections;
using System.Collections.Generic;
 using SimpleJSON;
 using UnityEngine;

public class DatosPregunta : MonoBehaviour
{

    public string nombrePregunta;
    public string idPregunta;
    public string descripcion;
    public string nombre;
    public string msgCorrecto;
    public string msgIncorrecto;
    public bool multi;
    public JSONNode opciones;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
