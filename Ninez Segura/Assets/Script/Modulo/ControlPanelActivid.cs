﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class ControlPanelActivid : MonoBehaviour
{
    public Text moduloNombre;
    public Text moduloMision;

    public GameObject panelModulo;
    public GameObject panelActivid;
    public GameObject modulo1;
    public GameObject modulo2;
    public GameObject modulo3;
    public GameObject modulo4;
    public GameObject modulo5;
    public GameObject modulo6;
    public GameObject modulo7;
    public GameObject modulo8;

    //public GameObject pregunta4;
    public Text texto1;
    public Text texto2;
    public Text texto3;
    public Text texto4;
    public Text texto5;
    public Text texto6;
    public Text texto7;
    public Text texto8;

    public Text unidadTitulo;

    public GameObject pregunta1;
    public GameObject pregunta2;
    public GameObject pregunta3;

    public Text preguntaTexto1;
    public Text preguntaTexto2;
    public Text preguntaTexto3;

    public Text preguntaId1;
    public Text preguntaId2;
    public Text preguntaId3;

    public GameObject modulo;
    public GameObject moduloUnidad;
    public GameObject moduloPregunta;


    // Start is called before the first frame update
    void Start()
    {
        //   al dar click en el modulo seteas static aqui pa saber q modulo es 
        // todo futuro no hace falta dos for basta con hacer una lista comparando el idModulo
        int i = 0;
        var moduloObj = modulo.GetComponent<DatosModuloElegido>();

        JSONNode data = CargarElementos.getUnidades();
        foreach (JSONNode unidad in data)
        {
            if (unidad["idModulo"] == moduloObj.idModulo)
            {
                i++;
            }
        }

        string[] nombresUni = new string[i];
        i = 0;
        foreach (JSONNode unidad in data)
        {
            if (unidad["idModulo"] == moduloObj.idModulo)
            {
                nombresUni[i] = unidad["nombre"];
                i++;
            }
        }

        // desactivo todos los vagones

        modulo1.SetActive(false);
        modulo2.SetActive(false);
        modulo3.SetActive(false);
        modulo4.SetActive(false);
        modulo5.SetActive(false);
        modulo6.SetActive(false);
        modulo7.SetActive(false);
        modulo8.SetActive(false);

        switch (i)
        {
            case 1:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(false);
                modulo3.SetActive(false);
                modulo4.SetActive(false);
                modulo5.SetActive(false);
                modulo6.SetActive(false);
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 2:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(false);
                modulo4.SetActive(false);
                modulo5.SetActive(false);
                modulo6.SetActive(false);
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 3:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(false);
                modulo5.SetActive(false);
                modulo6.SetActive(false);
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 4:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(true);
                texto4.text = nombresUni[3];
                modulo5.SetActive(false);
                modulo6.SetActive(false);
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 5:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(true);
                texto4.text = nombresUni[3];
                modulo5.SetActive(true);
                texto5.text = nombresUni[4];
                modulo6.SetActive(false);
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 6:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(true);
                texto4.text = nombresUni[3];
                modulo5.SetActive(true);
                texto5.text = nombresUni[4];
                modulo6.SetActive(true);
                texto6.text = nombresUni[5];
                modulo7.SetActive(false);
                modulo8.SetActive(false);
                break;
            case 7:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(true);
                texto4.text = nombresUni[3];
                modulo5.SetActive(true);
                texto5.text = nombresUni[4];
                modulo6.SetActive(true);
                texto6.text = nombresUni[5];
                modulo7.SetActive(true);
                texto7.text = nombresUni[6];
                modulo8.SetActive(false);
                break;
            case 8:
                modulo1.SetActive(true);
                texto1.text = nombresUni[0];
                modulo2.SetActive(true);
                texto2.text = nombresUni[1];
                modulo3.SetActive(true);
                texto3.text = nombresUni[2];
                modulo4.SetActive(true);
                texto4.text = nombresUni[3];
                modulo5.SetActive(true);
                texto5.text = nombresUni[4];
                modulo6.SetActive(true);
                texto6.text = nombresUni[5];
                modulo7.SetActive(true);
                texto7.text = nombresUni[6];
                modulo8.SetActive(true);
                texto8.text = nombresUni[7];
                break;
            default:
                // code block
                break;
        }

        panelActivid.SetActive(false);

        moduloNombre.text = moduloObj.nombreModulo;
        moduloMision.text = moduloObj.mision;

        var idUnidad = moduloUnidad.GetComponent<DatosUnidad>().idUnidad;
        if (idUnidad != null && idUnidad.Length > 1) // vengo desde preguntas entonces abro unidad
        {
            AbrirModalPreguntas(idUnidad);
        }
    }

    public void abrirModalPreguntas(Text unidadId)
    {
        AbrirModalPreguntas(unidadId.text);
    }

    private void AbrirModalPreguntas(string unidadId)
    {
        // panelModulo.SetActive(false); 
        string idUnidad = "";
        JSONNode dataUni = CargarElementos.getUnidades();
        foreach (JSONNode uni in dataUni)
        {
            if (uni["nombre"] == unidadId || uni["id"] == unidadId)
            {
                idUnidad = uni["id"];

                moduloUnidad.GetComponent<DatosUnidad>().idUnidad = uni["id"];
                moduloUnidad.GetComponent<DatosUnidad>().nombreUnidad = uni["nombre"];
                unidadTitulo.text = uni["nombre"];
            }
        }

        int i = 0;
        JSONNode data = CargarElementos.getPreguntas();
        foreach (JSONNode pre in data)
        {
            if (pre["idUnidad"] == idUnidad)
            {
                i++;
            }
        }

        string[,] preguntaNombreId = new string[i, 2];
        i = 0;

        foreach (JSONNode pre in data)
        {
            if (pre["idUnidad"] == idUnidad)
            {
                preguntaNombreId[i, 0] = pre["id"];
                preguntaNombreId[i, 1] = pre["tema"];
                i++;
                //Debug.Log("Pregunta nombre ::" + pre["nombre"] +"Valor i = "+i);
            }
        }

        switch (i)
        {
            case 1:
                pregunta1.SetActive(true);
                preguntaTexto1.text = preguntaNombreId[0, 1];
                preguntaId1.text = preguntaNombreId[0, 0];
                
                pregunta2.SetActive(false);
                pregunta3.SetActive(false);
                preguntaTexto2.gameObject.SetActive(false);
                preguntaTexto3.gameObject.SetActive(false);
                break; 
            case 2:
                pregunta1.SetActive(true);
                preguntaTexto1.text = preguntaNombreId[0, 1];
                preguntaId1.text = preguntaNombreId[0, 0];
                pregunta2.SetActive(true);
                preguntaTexto2.text = preguntaNombreId[1, 1];
                preguntaId2.text = preguntaNombreId[1, 0];
                
                pregunta3.SetActive(false);
                preguntaTexto3.gameObject.SetActive(false);
                break;
            case 3:
                pregunta1.SetActive(true);
                preguntaTexto1.text = preguntaNombreId[0, 1];
                preguntaId1.text = preguntaNombreId[0, 0];
                pregunta2.SetActive(true);
                preguntaTexto2.text = preguntaNombreId[1, 1];
                preguntaId2.text = preguntaNombreId[1, 0];
                pregunta3.SetActive(true);
                preguntaTexto3.text = preguntaNombreId[2, 1];
                preguntaId3.text = preguntaNombreId[2, 0];
                //pregunta4.SetActive(false);

                preguntaTexto3.gameObject.SetActive(true);
                break;
            case 4:
                pregunta1.SetActive(true);
                preguntaTexto1.text = preguntaNombreId[0, 1];
                preguntaId1.text = preguntaNombreId[0, 0];
                pregunta2.SetActive(true);
                preguntaTexto2.text = preguntaNombreId[1, 1];
                preguntaId2.text = preguntaNombreId[1, 0];
                pregunta3.SetActive(true);
                preguntaTexto3.text = preguntaNombreId[2, 1];
                preguntaId3.text = preguntaNombreId[2, 0];
                // pregunta4.SetActive(true);
                // preguntaTexto4.text = preguntaNombreId[3,1];
                // preguntaId4.text = preguntaNombreId[3,0];
                break;
            default:
                // code block
                break;
        }

        panelActivid.SetActive(true);
    }

    public void abrirPregunta(Text idPregunta)
    {
        Debug.Log("Pregunta id :  " + idPregunta.text);
        JSONNode data = CargarElementos.getPreguntas();
        foreach (JSONNode pre in data)
        {
            if (pre["id"] == idPregunta.text)
            {
                moduloUnidad.GetComponent<DatosUnidad>().idUnidad = pre["idUnidad"];
                moduloPregunta.GetComponent<DatosPregunta>().idPregunta = pre["id"];
                moduloPregunta.GetComponent<DatosPregunta>().msgCorrecto = pre["msgCorrecto"];
                moduloPregunta.GetComponent<DatosPregunta>().msgCorrecto = pre["msgCorrecto"];
                moduloPregunta.GetComponent<DatosPregunta>().opciones = pre["opciones"];
                moduloPregunta.GetComponent<DatosPregunta>().descripcion = pre["descripcion"];
                moduloPregunta.GetComponent<DatosPregunta>().nombre = pre["nombre"];
                moduloPregunta.GetComponent<DatosPregunta>().multi = pre["multi"].AsBool;

                if (pre["tipo"] == "OPCIONES")
                {
                    if (pre["tieneImagen"] == true)
                    {
                        SceneManager.LoadScene("JuegoDos");
                    }
                    else
                    {
                        SceneManager.LoadScene("JuegoUn");
                    }
                }
                else
                {
                    SceneManager.LoadScene("Juego3");
                }
            }
        }
    }

    public void bttnCerrarPanActivUno()
    {
        //panelModulo.SetActive(true);
        panelActivid.SetActive(false);
    }
}