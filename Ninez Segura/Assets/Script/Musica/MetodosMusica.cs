﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetodosMusica : MonoBehaviour
{
    public AudioClip musica;
    public AudioSource fuenteMusica;
    void Start()
    {
        fuenteMusica = GetComponent<AudioSource>();
        fuenteMusica.clip = musica;
    }

    // Update is called once per frame
    void Update()
    {

        
    }

    public void musicaPlay()
    {
        
        fuenteMusica.Play();
    }
    public void musicaPause()
    {
       // fuenteMusica.clip = musica;
        fuenteMusica.Pause();
    }
    public void musicaStop()
    {
       // fuenteMusica.clip = musica;
        fuenteMusica.Stop();
    }
    public void musicaReplay()
    {
        fuenteMusica.clip = musica;
        fuenteMusica.Play();
    }
}
