﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentarAvatar : MonoBehaviour
{

    public GameObject tripElegidoPrefab;
    public GameObject listaTripPrefab;
    public RawImage avatarElegido;
    public Text nombreTripulante;

    // Start is called before the first frame update
    void Start()
    {
        avatarElegido.texture = tripElegidoPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido;
        nombreTripulante.text= tripElegidoPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
