﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PresentarAvatarCreado : MonoBehaviour
{

    public GameObject panlTrip1;
    public GameObject panlTrip2;
    public GameObject panlTrip3;
    public GameObject panlTrip4;

    public RawImage imgTrip1;
    public RawImage imgTrip2;
    public RawImage imgTrip3;
    public RawImage imgTrip4;

    public Text nomTrip1;
    public Text nomTrip2;
    public Text nomTrip3;
    public Text nomTrip4;
    //prefabs de la lista de texturas y de la lista de tripulantes creados
    public GameObject listTripPrefab;
    public GameObject tripElegidPrefab;
    //
    public int index;

    // Start is called before the first frame update
    void Start()
    {
        index = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes.Count;
        //Debug.Log("index"+index);
        if (index!=0)
        {
            for (int i=0;i <index;i++)
            {
                activarPanel((i+1));
                asignarTexturaRaw((i + 1), i);
            }

        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void activarPanel(int numPanel)
    {
        if (numPanel==1)
        {
            panlTrip1.SetActive(true);
        }
        if (numPanel == 2)
        {
            panlTrip2.SetActive(true);
        }
        if (numPanel == 3)
        {
            panlTrip3.SetActive(true);
        }
        if (numPanel == 4)
        {
            panlTrip4.SetActive(true);
        }
    }
    
    public void asignarTexturaRaw(int numRawTexture, int posTxtureRaw)
    {
        
        if (numRawTexture == 1)
        {
            imgTrip1.texture = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombreImag;
            nomTrip1.text= listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombre;
        }
        if (numRawTexture == 2)
        {
            imgTrip2.texture = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombreImag;
            nomTrip2.text = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombre;
        }
        if (numRawTexture == 3)
        {
            imgTrip3.texture = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombreImag;
            nomTrip3.text = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombre;
        }
        if (numRawTexture == 4)
        {
            imgTrip4.texture = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombreImag;
            nomTrip4.text = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[posTxtureRaw].nombre;
        }
        
    }

    public void selectTrip1()
    {
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[0].nombreImag;
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[0].nombre;
    }
    public void selectTrip2()
    {
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[1].nombreImag;
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[1].nombre;
    }
    public void selectTrip3()
    {
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido= listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[2].nombreImag;
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[2].nombre;
    }
    public void selectTrip4()
    {
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[3].nombreImag;
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido = listTripPrefab.GetComponent<ListaTripulantes>().listaTripulantes[3].nombre;
    }

}
