﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CrearPerfil : MonoBehaviour
{
    public RawImage imAvat1;
    public RawImage imAvat2;
    public RawImage imAvat3;

    public Toggle hombre;
    public Toggle mujer;
    public Toggle ninoNina;
    public Toggle madrePadre;
    public Toggle docente;

    // game inputs
    public Text txtNombre;
    public Text txtEdad;

    public GameObject listAvatarsPrefab;
    public GameObject tripElegidPrefab;

    public int avatarIndex;
    public int avatarMinIndex; // pa saber de la lista de 12 cual index

    // Start is called before the first frame update
    void Start()
    {
        hombre.isOn = true;
        mujer.isOn = false;
        ninoNina.isOn = true;
        madrePadre.isOn = false;
        docente.isOn = false;

        /// todo detectar sie stoy en modo crear o en modo editar y setear datos
    }

    // Update is called once per frame
    void Update()
    {
        //grabarEdad();
    }

    public void bttnCrearPerfil()
    {
        almacenarTripNI();
        SceneManager.LoadScene("Menu");
    }

    public void selectGeneroEdadRol()
    {
        var valEdad2 = calcEdad();

        // por defecto lista de avatars hombre
        ListaImagenes list = listAvatarsPrefab.GetComponents<ListaImagenes>()[0];

        if (mujer.isOn == true)
        {
            list = listAvatarsPrefab.GetComponents<ListaImagenes>()[1];
        }

        if (valEdad2 <= 6)
        {
            avatarMinIndex = -1;
            imAvat1.texture = list.textureAvatar[0];
            imAvat2.texture = list.textureAvatar[1];
            imAvat3.texture = list.textureAvatar[2];
        }
        else if (valEdad2 < 12)
        {
            avatarMinIndex = 2;
            imAvat1.texture = list.textureAvatar[3];
            imAvat2.texture = list.textureAvatar[4];
            imAvat3.texture = list.textureAvatar[5];
        }
        else
        {
            if (madrePadre.isOn) // pa saber si es rol madre/padre
            {
                avatarMinIndex = 5;
                imAvat1.texture = list.textureAvatar[6];
                imAvat2.texture = list.textureAvatar[7];
                imAvat3.texture = list.textureAvatar[8];
            }
            else // rol cuidador
            {
                avatarMinIndex = 8;
                imAvat1.texture = list.textureAvatar[9];
                imAvat2.texture = list.textureAvatar[10];
                imAvat3.texture = list.textureAvatar[11];
            }
        }
    }

    private void almacenarTripNI()
    {
        var nombre = txtNombre.text;
        var edad = calcEdad();
        var isMujer = mujer.isOn;
        var isPadres = madrePadre.isOn;

        txtNombre.text = "";

        // PlayerPrefs.SetString("edad", edad);

        // por defecto lista de avatars hombre
        ListaImagenes list = listAvatarsPrefab.GetComponents<ListaImagenes>()[0];

        if (mujer.isOn == true)
        {
            list = listAvatarsPrefab.GetComponents<ListaImagenes>()[1];
        }

        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().textuTripElegido = list.textureAvatar[avatarIndex];
        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().nombreTripElegido = nombre;

        string categoria;

        if (edad <= 8)
        {
            categoria = "NIÑOS_6_A_8_AÑOS";
        }
        else if (edad <= 12)
        {
            categoria = "NIÑOS_9_A_12_AÑOS";
        }
        else if (madrePadre.isOn)
        {
            categoria = "PADRES";
        }
        else
        {
            categoria = "DOCENTES";
        }

        tripElegidPrefab.GetComponent<DatosTripulanteElegido>().categoria = categoria;


        //todo persisitir en disco lista de tripulantes agregado este nuevo y validar q no haya creado otro con el mismo nombre
    }

    public void selecAvatar(int opc)
    {
        avatarIndex = avatarMinIndex + opc;
    }

    private int calcEdad()
    {
        try
        {
            string edad = txtEdad.text;
            int valEdad;
            if (int.TryParse(edad, out valEdad))
            {
                return valEdad;
            }
        }
        catch (Exception e)
        {
            Debug.Log("calc edad: " + e.Message);
        }

        return 5;
    }
}